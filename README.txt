Scoop-style recommended posts.  Read more about Scoop here:

http://en.wikipedia.org/wiki/Scoop_(software)

This module provides two blocks that you can enable -- one that will appear on those node types you have enabled recommendations for, and one that lists the recent most recommended posts based on the Scoop formula.
