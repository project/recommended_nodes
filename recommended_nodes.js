
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    recommendedNodesAutoAttach();
  });
}

function recommendedNodesAutoAttach() {
  // Hide recommendors
  $('#recommenders').toggleClass('collapsed');

  // Make recommenders heading into a link
  $('#recommenders-title').click(function() {
    $('#recommenders').toggleClass('collapsed');
    $('#recommenders-title').toggleClass('expanded');
  });

  // Redirect recommend button
  $('#recommend-node-form').submit(function() {
    $('#edit-recommend').attr('disabled', true);
    $('#edit-recommend').after('&nbsp;<span class="throbber">&nbsp;</span>');
    uri = $('#recommend-uri').val();
    var recommendSaved = function(data) {
      var result = Drupal.parseJson(data);
      $('span.throbber').remove();
      $('#edit-recommend').val(result['title']);
      $('#edit-recommend').attr('disabled', false);
      $('#recommenders').html(result['recommenders']);
    };
    $.get(uri, null, recommendSaved);
    return false;
  });
}

